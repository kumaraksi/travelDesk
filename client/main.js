$('#login-form').on('submit', function(e) {
    e.preventDefault();
    var loginForm = $(this);
    $.ajax({
        type: loginForm.attr('method'),
        url: loginForm.attr('action'),
        data: loginForm.serialize(),
        success: function(response) {
            window.location.pathname = response.redirect;
        }
    });
})

// using context
$('.ui.sidebar')
    .sidebar('setting', 'transition', 'overlay')
    .sidebar('attach events', '#toggleSidebar');

$('.ui.dropdown')
    .dropdown();