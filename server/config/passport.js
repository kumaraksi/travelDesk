/**
 *Module dependencies
 */
var
    passport = require('passport'),
    config = require('./config'),
    models = require('../models');
/**
 *Module variables
 */
var
    host = config.host,
    LocalStrategy = require('passport-local').Strategy;
/**
 *Configuration and Settings
 */
passport.serializeUser(function(user, done) {
    done(null, user.id)
});

passport.deserializeUser(function(id, done) {
    models.User.findOne({
        where: {
            'id': id
        }
    }).then(function(user) {
        if (user == null) {
            done(new Error('Wrong user id.'))
        }

        done(null, user)
    })
});
/**
 *Strategies
 */
//---------------------------Local Strategy-------------------------------------
passport.use('local-signup', new LocalStrategy(
    function(username, password, done) {
        process.nextTick(function() {
            models.User.findOne({ username: username }, function(err, user) {
                if (err) {
                    return console.error(err.message);
                }
                if (user) {
                    console.log('user already exists');
                    return done(null, false, { errMsg: 'email already exists' });
                } else {
                    var newUser = new User();
                    newUser.username = req.body.username;
                    newUser.name = name;
                    newUser.password = newUser.generateHash(password);
                    newUser.save(function(err) {
                        if (err) {
                            console.log(err);
                            if (err.message == 'User validation failed') {
                                console.log(err.message);
                                return done(null, false, { errMsg: 'Please fill all fields' });
                            }
                            return console.error(err.message);
                        }
                        console.log('New user successfully created...', newUser.username);
                        console.log('email', email);
                        console.log(newUser);
                        return done(null, newUser);
                    });
                }
            });
        });
    }));
//---------------------------local login----------------------------------------

passport.use('local-login', new LocalStrategy(
    function(username, password, done) {
        process.nextTick(function() {
            models.User.findOne({
                where: {
                    'username': username
                }
            }).then(function(user) {
                if (user == null) {
                    return done(null, false, { message: 'Incorrect credentials.' })
                }

                //var hashedPassword = bcrypt.hashSync(password, user.salt)

                if (user.password === password) {
                    return done(null, user)
                }

                return done(null, false, { message: 'Incorrect credentials.' })
            }).catch(function(err) {
                return console.error(err);
            })
        });
    }
));
/**
 *Export Module
 */
module.exports = passport;