var express = require('express');
var router = express.Router();
var passport = require('../config/passport');

function isLoggedIn(req, res, next) {
    if (req.isAuthenticated()) {
        return next();
    }
    return res.redirect('/');
}

router.get('/', (req, res) => {
    var stream = res.push('pages/index', {
        status: 200, // optional
        method: 'GET', // optional
        request: {
            accept: '*/*'
        },
        response: {
            'content-type': 'text/html'
        }
    });
    stream.on('error', function() {});
    stream.end('alert("hello from push stream!");');
    res.render('./pages/index');
});

router.get('/home', isLoggedIn, (req, res) => {
    res.render('./pages/home');
});


//Login Manager
var loginManager = require('./loginManager');
router.post('/api/login', passport.authenticate('local-login'), loginManager.login);
router.post('/api/signup', loginManager.signup);
router.get('/api/logout', loginManager.logout);

module.exports = router;