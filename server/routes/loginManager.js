const client = '../client';
var session;

function login(req, res, next) {
    if (true) {
        //sess.email = req.body.username;
        res.status(200)
            .json({
                redirect: '/home',
                result: "succcess"
            });
    } else {
        res.json(200, {
            redirect: '/',
            result: "error",
            message: "Invalid username or password"
        });
        res.end();
    }
}

function signup(req, res, next) {
    console.info(req);
}

function logout(req, res, next) {
    req.logout();
    req.session.destroy();
    return res.redirect('/');
}

module.exports = {
    login: login,
    signup: signup,
    logout: logout
};