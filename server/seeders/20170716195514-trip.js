'use strict';
var faker = require('faker');

module.exports = {
    up: function(queryInterface, Sequelize) {
        /*
          Add altering commands here.
          Return a promise to correctly handle asynchronicity.

          Example:
          return queryInterface.bulkInsert('Person', [{
            name: 'John Doe',
            isBetaMember: false
          }], {});
        */
        var trips = [];
        for (var i = 0; i < 100; i++) {
            var trip = {};
            trip.from = faker.address.streetName() + "," + faker.address.streetAddress() + "," + faker.address.city();
            trip.to = faker.address.streetName() + "," + faker.address.streetAddress() + "," + faker.address.city();
            trip.reportingTime = faker.date.between('2017-01-01', '2017-12-31');
            trip.vehicleId = faker.random.number({
                'min': 1,
                'max': 10
            });
            trip.driverId = faker.random.number({
                'min': 1,
                'max': 10
            });
            trip.passengerId = faker.random.number({
                'min': 1,
                'max': 50
            });

            trips.push(trip);
        }
        return queryInterface.bulkInsert('trips', trips, {});
    },

    down: function(queryInterface, Sequelize) {
        /*
          Add reverting commands here.
          Return a promise to correctly handle asynchronicity.

          Example:
          return queryInterface.bulkDelete('Person', null, {});
        */
        queryInterface.bulkDelete('trips', null, {});
    }
};