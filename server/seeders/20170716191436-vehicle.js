'use strict';
var faker = require('faker');

module.exports = {
    up: function(queryInterface, Sequelize) {
        /*
          Add altering commands here.
          Return a promise to correctly handle asynchronicity.

          Example:
          return queryInterface.bulkInsert('Person', [{
            name: 'John Doe',
            isBetaMember: false
          }], {});
        */
        var vehicles = [];
        for (var i = 0; i < 10; i++) {
            var vehicle = {};
            vehicle.name = faker.random.arrayElement(["Dezire", "Indica", "Indigo", "Etios", "Innova", "Fortuner", "bus"]);
            vehicle.number = faker.helpers.replaceSymbolWithNumber("MH-####");
            vehicle.fuelType = faker.random.number({
                'min': 0,
                'max': 2
            });
            vehicle.seater = faker.random.number({
                'min': 1,
                'max': 30
            });
            vehicles.push(vehicle);
        }
        return queryInterface.bulkInsert('vehicles', vehicles, {});
    },

    down: function(queryInterface, Sequelize) {
        /*
          Add reverting commands here.
          Return a promise to correctly handle asynchronicity.

          Example:
          return queryInterface.bulkDelete('Person', null, {});
        */
        queryInterface.bulkDelete('vehicles', null, {});
    }
};