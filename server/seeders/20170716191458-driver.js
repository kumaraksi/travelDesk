'use strict';
var faker = require('faker');

module.exports = {
    up: function(queryInterface, Sequelize) {
        /*
          Add altering commands here.
          Return a promise to correctly handle asynchronicity.

          Example:
          return queryInterface.bulkInsert('Person', [{
            name: 'John Doe',
            isBetaMember: false
          }], {});
        */
        var drivers = [];
        for (var i = 0; i < 10; i++) {
            var driver = {};
            driver.name = faker.name.findName();
            driver.address = faker.address.streetName() + "," + faker.address.streetAddress() + "," + faker.address.city() + "," + faker.address.state();
            driver.mobile = faker.random.number({
                min: 1000000000,
                max: 9999999999
            });
            driver.vehicleId = faker.random.number({
                'min': 1,
                'max': 10
            });
            drivers.push(driver);
        }
        return queryInterface.bulkInsert('drivers', drivers, {});
    },

    down: function(queryInterface, Sequelize) {
        /*
          Add reverting commands here.
          Return a promise to correctly handle asynchronicity.

          Example:
          return queryInterface.bulkDelete('Person', null, {});
        */
        queryInterface.bulkDelete('drivers', null, {});
    }
};