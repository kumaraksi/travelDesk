'use strict';
var faker = require('faker');

module.exports = {
    up: function(queryInterface, Sequelize) {
        /*
          Add altering commands here.
          Return a promise to correctly handle asynchronicity.

          Example:
          return queryInterface.bulkInsert('Person', [{
            name: 'John Doe',
            isBetaMember: false
          }], {});
        */
        var passengers = [];
        for (var i = 0; i < 50; i++) {
            var passenger = {};
            passenger.name = faker.name.findName();
            passenger.address = faker.address.streetName() + "," + faker.address.streetAddress() + "," + faker.address.city() + "," + faker.address.state();
            passenger.mobile = faker.random.number({
                min: 1000000000,
                max: 9999999999
            });
            passenger.companyId = faker.random.number({
                'min': 1,
                'max': 4
            });
            passengers.push(passenger);
        }
        return queryInterface.bulkInsert('passengers', passengers, {});

    },

    down: function(queryInterface, Sequelize) {
        /*
          Add reverting commands here.
          Return a promise to correctly handle asynchronicity.

          Example:
          return queryInterface.bulkDelete('Person', null, {});
        */
        queryInterface.bulkDelete('passengers', null, {});
    }
};