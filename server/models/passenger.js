'use strict';
module.exports = function(sequelize, DataTypes) {
    var passenger = sequelize.define('passenger', {
        name: {
            type: DataTypes.STRING,
            allowNull: false
        },
        address: {
            type: DataTypes.TEXT,
            allowNull: false
        },
        mobile: {
            type: DataTypes.BIGINT,
            allowNull: false
        }
    }, {
        classMethods: {
            associate: function(models) {
                // associations can be defined here
                passenger.belongsTo(models.Company, {
                    foreignKey: 'companyId',
                    onDelete: 'CASCADE',
                });
                passenger.hasMany(models.trip, {
                    foreignKey: 'tripId',
                    as: 'tripPassenger',
                });

            }
        }
    });
    return passenger;
};