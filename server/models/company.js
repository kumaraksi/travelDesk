'use strict';
module.exports = function(sequelize, DataTypes) {
    var Company = sequelize.define('Company', {
        name: DataTypes.STRING,
        allowNull: false
    }, {
        classMethods: {
            associate: function(models) {
                // associations can be defined here
                Company.hasMany(models.passenger, {
                    foreignKey: 'companyId',
                    as: 'companyEmployees',
                });
            }
        }
    });
    return Company;
};