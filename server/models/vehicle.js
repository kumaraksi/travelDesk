'use strict';
module.exports = function(sequelize, DataTypes) {
    var vehicle = sequelize.define('vehicle', {
        name: {
            type: DataTypes.STRING,
            allowNull: false
        },
        number: {
            type: DataTypes.STRING,
            allowNull: false
        },
        fuelType: {
            type: DataTypes.INTEGER, //0 - LPG 1 - PETROL 2 - DIESEL
            allowNull: false
        },
        seater: {
            type: DataTypes.INTEGER,
            allowNull: false
        }
    }, {
        classMethods: {
            associate: function(models) {
                // associations can be defined here
                vehicle.hasOne(models.driver, {
                    foreignKey: 'vehicleId',
                    as: 'vehicleDriver',
                });
                vehicle.hasMany(models.trip, {
                    foreignKey: 'tripId',
                    as: 'tripVehicle',
                });


            }
        }
    });
    return vehicle;
};