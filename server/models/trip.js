'use strict';
module.exports = function(sequelize, DataTypes) {
    var trip = sequelize.define('trip', {
        from: {
            type: DataTypes.STRING,
            allowNull: false
        },
        to: {
            type: DataTypes.STRING,
            allowNull: false
        },
        reportingTime: {
            type: DataTypes.DATE,
            allowNull: false
        }
    }, {
        classMethods: {
            associate: function(models) {
                // associations can be defined here
                trip.belongsTo(models.passenger, {
                    foreignKey: 'passengerId',
                    onDelete: 'CASCADE',
                });
                trip.belongsTo(models.vehicle, {
                    foreignKey: 'vehicleId',
                    onDelete: 'CASCADE',
                });
                trip.belongsTo(models.driver, {
                    foreignKey: 'driverId',
                    onDelete: 'CASCADE',
                });

            }
        }
    });
    return trip;
};