'use strict';
module.exports = function(sequelize, DataTypes) {
    var driver = sequelize.define('driver', {
        name: {
            type: DataTypes.STRING,
            allowNull: false
        },
        address: {
            type: DataTypes.STRING,
            allowNull: false
        },
        mobile: {
            type: DataTypes.BIGINT,
            allowNull: false
        }
    }, {
        classMethods: {
            associate: function(models) {
                // associations can be defined here
                driver.belongsTo(models.vehicle, {
                    foreignKey: 'vehicleId',
                    onDelete: 'CASCADE',
                });
                driver.hasMany(models.trip, {
                    foreignKey: 'tripId',
                    as: 'tripDriver',
                });
            }
        }
    });
    return driver;
};