const spdy = require('spdy');
const express = require('express');
const path = require('path');
const fs = require('fs');
const logger = require('morgan');
const bodyParser = require("body-parser");
const cookieParser = require("cookie-parser");
const session = require("express-session");
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const config = require('./server/config/config');

const app = express();


var routes = require('./server/routes/index');

app.use(logger("dev"));

app.set('view engine', 'ejs');

app.set('views', __dirname + '/client');

app.use(express.static('client'));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded());
app.use(cookieParser());
app.use(session({ secret: config.sessionSecret, key: config.sessionKey }));
app.use(passport.initialize());
app.use(passport.session());
//////////////////
// API Queries
//////////////////

app.use("/", routes);


//////////////////
// Server Setup
//////////////////

app.set("env", config.env);
app.set("host", config.host);
app.set("port", config.port);



// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        console.log(err);
        res.status(err.code || 500)
            .json({
                status: 'error',
                message: err
            });
    });
}

// app.get('*', (req, res) => {
//     res
//         .status(200)
//         .json({ message: 'ok' })
// });

const options = {
    key: fs.readFileSync(__dirname + '/server.key'),
    cert: fs.readFileSync(__dirname + '/server.crt')
}

spdy
    .createServer(options, app)
    .listen(app.get('port'), (error) => {
        if (error) {
            console.error(error)
            return process.exit(1)
        } else {
            console.log('Listening on port: ' + app.get('port') + '.');
        }
    });

module.exports = app;